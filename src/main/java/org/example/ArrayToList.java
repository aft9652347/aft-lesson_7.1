package org.example;

import java.util.*;

public class ArrayToList {
    public static void main(String[] args) {
        String[] arrayColorsRainbow = {"красный", "оранжевый", "желтый", "зеленый", "голубой", "синий", "фиолетовый"};

        List<String> listColorsRainbow = new ArrayList<>(Arrays.asList(arrayColorsRainbow));

        //Вывод списка через итератор
        Iterator<String> iterator = listColorsRainbow.iterator();
        while (iterator.hasNext()) {
            String color = iterator.next();
            System.out.println(color);
        }

        //Сортируем список по алфавиту
        Collections.sort(listColorsRainbow);
//        System.out.println(listColorsRainbow);

        //Заменяем элементы в списке на аналогичные, но с приставкой Dark (специально оставил на русском)
        listColorsRainbow.set(0, "Dark голубой");
        listColorsRainbow.set(2, "Dark зелёный");
        listColorsRainbow.set(4, "Dark оранжевый");
//        System.out.println(listColorsRainbow);

        //Проходимся по элементам списка при помощи цикла хотя под капотом там тот-же итератором
        for (String rainbowColor : listColorsRainbow) {
            System.out.println(rainbowColor);
        }

        //Вывести все элементы списка через for
//        for (int i = 0; i < listColorsRainbow.size(); i++) {
//            System.out.println(listColorsRainbow.get(i));
//        }

        //Получение подсписка с 1 по 5 элемент
        for (int i = 0; i < 5; i++) {
            List<String> list = new ArrayList<>();
            list.add(listColorsRainbow.get(i));
//            System.out.println(list);
        }

        //Метод для перестановки 1 и 4 элемента местами
        Collections.swap(listColorsRainbow, 1, 4);
//        System.out.println(listColorsRainbow);

        //Сохраняем элемент с индексом 3 в переменную "a1"
        String a1 = listColorsRainbow.get(3);
//        System.out.println(a1);

        //Проверить содержится элемент из переменной "a1" в коллекции
        Iterator<String> searschСollorIterator = listColorsRainbow.iterator();
        while (searschСollorIterator.hasNext()) {
            String searchColor = searschСollorIterator.next();
            if (searchColor.equals(a1)) {
                System.out.println("Проверка прошла успешно, в списке присутствует искомый элемент");
            }
        }

        //Удалить все элементы содержащие букву "о" спасибо итератор
        Iterator<String> collorIterator = listColorsRainbow.iterator();
        while (collorIterator.hasNext()) {
            String color = collorIterator.next();
            if (color.contains("о")) {
                collorIterator.remove();
//                System.out.println(color);
            }
        }
//        System.out.println("новый список: " + listColorsRainbow);

        //преобразуем список в массив
        String[] arrayColorsRainbow2 = listColorsRainbow.toArray(new String[0]);

//        for (String printArrayColorsRainbow2 : arrayColorsRainbow2) {
//            System.out.println("массив:" + printArrayColorsRainbow2);
//        }
    }
}